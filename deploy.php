<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'todo-laravel');

// Project repository
set('repository', 'git@gitlab.com:prog-0485-ajorda/todo-laravel-despliege.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('192.168.56.103') ->user('ddaw-ud4-deploye')
                       ->identityFile('~/.ssh/id_rsa.pub')
                       ->set('deploy_path', '/var/www/ddaw-ud4-a4/html');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

# Declaració de la tasca
task('reload:php-fpm', function () {
  run('sudo /etc/init.d/php8.1-fpm restart');
});
# inclusió en el cicle de desplegament
after('deploy', 'reload:php-fpm');
